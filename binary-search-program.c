#include <stdio.h>

#define MAX 25

// array of items on which linear search will be conducted.
int intArray[MAX] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};

void printline(int count) {
    int i;

    for (i = 0; i < count - 1; i++) {
        printf("=");
    }

    printf("=\n");
}

int find(int data) {
    int lowerBound = 0;
    int upperBound = MAX - 1;
    int midPoint = -1;
    int comparisons = 0;
    int index = -1;

    while (lowerBound <= upperBound) {
        printf("Comparison %d\n", (comparisons + 1));
        printf("lowerBound : %d, intArray[%d] = %d\n", lowerBound, lowerBound,
               intArray[lowerBound]);
        printf("upperBound : %d, intArray[%d] = %d\n", upperBound, upperBound,
               intArray[upperBound]);
        comparisons++;

        // compute the mid point
        // midPoint = (lowerBound + upperBound) / 2;
        midPoint = lowerBound + (upperBound - lowerBound) / 2;

        // data found
        if (intArray[midPoint] == data) {
            index = midPoint;
            break;
        } else {
            // if data is larger
            if (intArray[midPoint] < data) {
                // data is in upper half
                lowerBound = midPoint + 1;
            }
                // data is smaller
            else {
                // data is in lower half
                upperBound = midPoint - 1;
            }
        }
    }
    printf("Total comparisons made: %d", comparisons);
    return index;
}

void display() {
    int i;
    printf("[");

    // navigate through all items
    for (i = 0; i < MAX; i++) {
        printf("%d ", intArray[i]);
    }

    printf("]\n");
}

main() {
    printf("Input Array: ");
    display();
    printline(50);

    //find location of 1
    int location = find(59);

    // if element was found
    if (location != -1)
        printf("\nElement found at location: %d", (location + 1));
    else
        printf("\nElement not found.");
}