#include <stdio.h>

int main() {
    int size, i;
    float sum = 0.0, avg;

    printf("Enter the size of an Array: ");
    scanf("%d", &size);

    int A[size];
    printf("Enter %d numbers to calculate an average of them: ", size);
    printf("\n");

    for (i = 0; i < size; i++) {
        scanf("%d", &A[i]);
        sum += A[i];
    }
    avg = sum / size;

    printf("\nThe average value of %d numbers are: %.2f\n", size, avg);

    return 0;
}