#include <stdio.h>

int main() {
    int size, i, sElement;
    printf("Enter the list size (integer number): ");
    scanf("%d", &size);
    int lis[size];

    printf("Enter %d integer values: ", size);
    printf("\n");

    for (i = 0; i < size; i++)
        scanf("%d", &lis[i]);

    printf("Enter any element to search: ");
    printf("\n");

    scanf("%d", &sElement);
    for (i = 0; i < size; i++) {
        if (sElement == lis[i]) {
            printf("Success, The element has found at INDEX #%d\n", i);
            break;
        }
    }

    if (i == size)
        printf("Sorry, The given value not found on the list.\n");

    return 0;
}